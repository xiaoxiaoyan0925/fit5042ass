package fit5042.ass.userControllers;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.*;

/**
 * 
 * @author Yan
 *
 */

@RequestScoped
@Named(value = "user")
public class User implements Serializable{

	private int id;
	@Size(min = 3, message = "Name should at least be 3 characters long")
	private String username;
	@NotBlank(message = "Password cannot be null")
	private String password;
	@NotBlank(message = "Groupname cannot be null")
	private String groupname;
	
	public User() {
	}
	
	public User(int id, String username, String password, String groupname) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.groupname = groupname;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	//toString method
	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", groupname=" + groupname
				+ "]";
	}

	
}
