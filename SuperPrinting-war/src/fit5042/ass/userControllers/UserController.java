package fit5042.ass.userControllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.ass.userControllers.UserApplication;

/**
 * 
 * @author Yan
 *
 */

@Named(value = "userController")
@RequestScoped
public class UserController {

	private int id;
	private fit5042.ass.repository.entities.Users user;
	
	public UserController() {
		id = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("userID"));
        // Assign property based on the id provided 
        user = getUser();
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public fit5042.ass.repository.entities.Users getUser() {
        if (user == null) {
            ELContext context
                    = FacesContext.getCurrentInstance().getELContext();
            UserApplication userApplication
                    = (UserApplication) FacesContext.getCurrentInstance()
                            .getApplication()
                            .getELResolver()
                            .getValue(context, null, "userApplication");
            // -1 to userId to have positive user id
            return userApplication.getUsers().get(--id); 
        }
        return user;
    }
}
