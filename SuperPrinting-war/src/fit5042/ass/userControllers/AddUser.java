package fit5042.ass.userControllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import fit5042.ass.mbeans.UserManagedBean;

/**
 * 
 * @author Yan
 *
 */
@RequestScoped
@Named("addUser")
public class AddUser {

	@Inject
	UserManagedBean userManagedBean;	
	private boolean showForm = true;
	private User user;   
    UserApplication app;
    
	public UserManagedBean getUserManagedBean() {
		return userManagedBean;
	}
	public void setUserManagedBean(UserManagedBean userManagedBean) {
		this.userManagedBean = userManagedBean;
	}
	public boolean isShowForm() {
		return showForm;
	}
	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public UserApplication getApp() {
		return app;
	}
	public void setApp(UserApplication app) {
		this.app = app;
	}
    
	public AddUser() 
    {
    	ELContext elContext1 = FacesContext.getCurrentInstance().getELContext();
        app = (UserApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(elContext1, null, "userApplication"); 
        app.updateUserList();
        
        //instantiate customerManagedBean
        ELContext elContext2 = FacesContext.getCurrentInstance().getELContext();
        userManagedBean = (UserManagedBean) FacesContext.getCurrentInstance()
        														.getApplication()
        														.getELResolver()
        														.getValue(elContext2, null, "userManagedBean");
    }
    
	public void addUser(User localUser) {
        //this is the local user
       try
       {
            //add this user to db via EJB
    	   userManagedBean.addUser(localUser);

            //refresh the list in UserApplication bean
            app.searchAll();
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User has been added succesfully"));
       }
       catch (Exception ex)
       {
           
       }
        showForm = true;
    }
}

