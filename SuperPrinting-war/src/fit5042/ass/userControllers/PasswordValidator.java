package fit5042.ass.userControllers;

import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;

@FacesValidator("passwordValidator")
public class PasswordValidator implements Validator {
	

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        UIInput passwordComponent = (UIInput) component.getAttributes().get("passwordComponent"); 
        String password = (String) passwordComponent.getValue(); 
        String confirm = (String) value; 
        
        if (password == null) {
        	throw new ValidatorException(new FacesMessage("Password are null."));
        }
        
        if (confirm == null) {
        	throw new ValidatorException(new FacesMessage("confirm are null."));
        }
  
        if (!password.equals(confirm)) {
            throw new ValidatorException(new FacesMessage("Passwords are not equal."));
        }
    }


}