package fit5042.ass.userControllers;

import java.util.ArrayList;
import java.util.List;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import fit5042.ass.mbeans.UserManagedBean;
import fit5042.ass.repository.entities.Users;

/**
 * 
 * @author Yan
 *
 */

@Named(value = "userApplication")
@ApplicationScoped
public class UserApplication {

	@Inject
    UserManagedBean userManagedBean;
	private List<Users> users;
    private boolean showForm = true;
    
	public UserManagedBean getUserManagedBean() {
		return userManagedBean;
	}
	public void setUserManagedBean(UserManagedBean userManagedBean) {
		this.userManagedBean = userManagedBean;
	}
	public List<Users> getUsers() {
		return users;
	}
	public void setUsers(List<Users> users) {
		this.users = users;
	}
	public boolean isShowForm() {
		return showForm;
	}
	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}
	
	public UserApplication() throws Exception { 
    	//instantiate user list
		users = new ArrayList<>();
        
        //instantiate userManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        userManagedBean = (UserManagedBean) FacesContext.getCurrentInstance().getApplication()
        				.getELResolver().getValue(elContext, null, "userManagedBean");
        
        //get users from db 
        updateUserList();
    }
	
	public void updateUserList() {
		if (users == null || users.size() <= 0)
        {
			users.clear();

            for (fit5042.ass.repository.entities.Users user : userManagedBean.getAllNormalUsers())
            {
            	users.add(user);
            }
            setUsers(users);
        }
		
	}
    
	public void searchUserById(int userId)
    {
		users.clear();
		users.add(userManagedBean.searchUserById(userId));
    }
	
	public void searchUserByName(String name) {
		users.clear();
		for (fit5042.ass.repository.entities.Users user : userManagedBean.searchUserByName(name))
        {
			users.add(user);
        }
        setUsers(users);
	}
	
	public void searchAll()
    {
		users.clear();    
		for (fit5042.ass.repository.entities.Users user : userManagedBean.getAllUsers())
        {
			users.add(user);
        }
        setUsers(users);
    }
	
	public void searchAllNormalUsers()
    {
		users.clear();    
		for (fit5042.ass.repository.entities.Users user : userManagedBean.getAllNormalUsers())
        {
			users.add(user);
        }
        setUsers(users);
    }
}
