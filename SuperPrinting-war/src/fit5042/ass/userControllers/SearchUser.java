package fit5042.ass.userControllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;


/**
*
* @author Yan
* 
*/

@RequestScoped
@Named("searchUser")
public class SearchUser {
	
	private User user;	   
	UserApplication userApplication;  
	UserApp userApp;
	private int searchById;
	private String searchByName;
	private int searchByUserid;
	
	public SearchUser() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        userApplication = (UserApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "userApplication");
        userApp = (UserApp) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "userApp");
        
        userApplication.updateUserList();
    }

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserApplication getApp() {
		return userApplication;
	}

	public void setApp(UserApplication app) {
		this.userApplication = app;
	}

	public UserApp getUserApp() {
		return userApp;
	}

	public void setUserApp(UserApp userApp) {
		this.userApp = userApp;
	}	
	
	public int getSearchById() {
		return searchById;
	}

	public void setSearchById(int searchById) {
		this.searchById = searchById;
	}

	public String getSearchByName() {
		return searchByName;
	}

	public void setSearchByName(String searchByName) {
		this.searchByName = searchByName;
	}

	public int getSearchByUserid() {
		return searchByUserid;
	}

	public void setSearchByUserid(int searchByUserid) {
		this.searchByUserid = searchByUserid;
	}

	public void searchUserById(int id) 
    {
       try
       {
            //search this user then refresh the list in UserApplication bean
    	   userApplication.searchUserById(id);
    	   userApp.searchUserById(id);
       }
       catch (Exception ex)
       {
           
       }
    }
    
    public void searchUserByName(String name)
    {
       try
       {
            //search this user then refresh the list in UserApplication bean
    	   userApplication.searchUserByName(name);
       }
       catch (Exception ex)
       {
           
       }
    }
    
    public void searchAll() 
    {
       try
       {
    	   userApplication.searchAll();
       }
       catch (Exception ex)
       {
           
       }
    }
    
    public void searchAllNormalUsers() 
    {
       try
       {
    	   userApplication.searchAllNormalUsers();
       }
       catch (Exception ex)
       {
           
       }
    }
}
