package fit5042.ass.userControllers;

import java.util.ArrayList;
import java.util.List;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import fit5042.ass.mbeans.Authentication;
import fit5042.ass.mbeans.UserManagedBean;
import fit5042.ass.repository.entities.Users;

/**
 * 
 * @author Yan
 *
 */

@Named(value = "userApp")
@ApplicationScoped
public class UserApp {

	@Inject
    UserManagedBean userManagedBean;
	@Inject
	Authentication authentication;
	
	public Authentication getAuthentication() {
		return authentication;
	}
	public void setAuthentication(Authentication authentication) {
		this.authentication = authentication;
	}

	private int id;
	private fit5042.ass.repository.entities.Users user;
    private boolean showForm = true;
    
	public UserManagedBean getUserManagedBean() {
		return userManagedBean;
	}
	public void setUserManagedBean(UserManagedBean userManagedBean) {
		this.userManagedBean = userManagedBean;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public fit5042.ass.repository.entities.Users getUser() {
		return user;
	}
	public void setUser(fit5042.ass.repository.entities.Users user) {
		this.user = user;
	}
	public boolean isShowForm() {
		return showForm;
	}
	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}
	
	public UserApp() throws Exception { 

        //instantiate userManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        userManagedBean = (UserManagedBean) FacesContext.getCurrentInstance().getApplication()
        				.getELResolver().getValue(elContext, null, "userManagedBean");
        
        authentication = (Authentication) FacesContext.getCurrentInstance().getApplication()
				.getELResolver().getValue(elContext, null, "authentication");
        
        //get users from db 
        user = authentication.getUser();
    }
	
	public void updateUserList() {
		if (user == null)
        {
            setUser(user);
        }
		
	}
    
	public void searchUserById(int userId)
    {
		setUser(userManagedBean.searchUserById(userId));
    }

}