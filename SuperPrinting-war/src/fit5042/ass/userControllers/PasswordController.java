package fit5042.ass.userControllers;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * 
 * @author Yan
 *
 */

@Named(value = "passwordController")
@RequestScoped
public class PasswordController {
	
	private String newPassword;
	private String confirmPassword;
	
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
	public String submitPassword(){
		if (newPassword.equals(confirmPassword))	
			return newPassword;
		return null;
	}
	

}
