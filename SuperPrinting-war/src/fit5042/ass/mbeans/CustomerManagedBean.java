package fit5042.ass.mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;

import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.ass.repository.CustomerRepository;
import fit5042.ass.repository.entities.Address;
import fit5042.ass.repository.entities.Customer;
import fit5042.ass.repository.entities.DataCommunicationCustomer;
import fit5042.ass.repository.entities.HealthCustomer;
import fit5042.ass.repository.entities.Contact;

/**
 * 
 * @author Yan
 *
 */

@Named(value = "customerManagedBean")
@SessionScoped
public class CustomerManagedBean implements Serializable{
	
	@EJB
	CustomerRepository customerRepository;

	public CustomerManagedBean() {
	}
	
    public List<Customer> getAllCustomers() {
        try {
        	List<Customer> customers = customerRepository.getAllCustomers();
            return customers;
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<DataCommunicationCustomer> getAllDcCustomers() {
        try {
        	List<DataCommunicationCustomer> customers = customerRepository.getAllDcCustomers();
            return customers;
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<HealthCustomer> getAllHeCustomers() {
        try {
        	List<HealthCustomer> customers = customerRepository.getAllHeCustomers();
            return customers;
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void setCustomerList(List<Customer> customerList) {
    	try {
    		customerRepository.setCustomerList(customerList);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
	}
    
    public void addCustomer(fit5042.ass.customerControllers.Customer localCustomer) 
    {
    	Customer customer = convertCustomerToEntity(localCustomer);
        try {
        	customerRepository.addCustomer(customer);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void editCustomer(Customer customer)
    {
        try {
            String streetNumber = customer.getAddress().getStreetNumber();
            String streetAddress = customer.getAddress().getStreetAddress();
            String suburb = customer.getAddress().getSuburb();
            String state = customer.getAddress().getState();
            String postcode = customer.getAddress().getPostcode();
            
            Address address = customer.getAddress();
            address.setStreetNumber(streetNumber);
            address.setStreetAddress(streetAddress);
            address.setSuburb(suburb);
            address.setState(state);
            address.setPostcode(postcode);
            
            customer.setAddress(address);
            
            customerRepository.editCustomer(customer);
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void removeCustomer(int customerId) 
    {
        try {
        	customerRepository.removeCustomer(customerId);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Customer searchCustomerById(int customerId)
    {
        try {
            return customerRepository.searchCustomerById(customerId);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Customer> searchCustomerByUserid(int userid)
    {
        try {
            return customerRepository.searchCustomerByUserid(userid);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Contact> getAllCustomerContacts() {
    	try {
            return customerRepository.getAllCustomerContacts();
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
		return null;
	}
    
    //...
    public List<Customer> searchCustomerByName(String name) {
    	try {
            return customerRepository.searchCustomerByName(name);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
		return null;
	}
    
    public List<Customer> searchCustomerByMultipleField(String field1, String value1, String field2, String value2) {
    	try {
            return customerRepository.searchCustomerByMultipleField(field1, value1, field2, value2);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
		return null;
	}
    
    private Customer convertCustomerToEntity(fit5042.ass.customerControllers.Customer localCustomer) {
    	
    	Customer customer = new Customer(); //entity
    	
        String streetNumber = localCustomer.getStreetNumber();
        String streetAddress = localCustomer.getStreetAddress();
        String suburb = localCustomer.getSuburb();
        String postcode = localCustomer.getPostcode();
        String state = localCustomer.getState();
        Address address = new Address(streetNumber, streetAddress, suburb, postcode, state);
        
        customer.setAddress(address);
        customer.setCustomerId(localCustomer.getCustomerId());
        customer.setName(localCustomer.getName());
        customer.setPhoneNumber(localCustomer.getPhoneNumber());
        customer.setEmail(localCustomer.getEmail());
        customer.setFax(localCustomer.getFax());
        customer.setDescription(localCustomer.getDescription());
        customer.setIndustryType(localCustomer.getIndustryType());
        customer.setUserid(localCustomer.getUserid());
        //customer.setCustomerContacts(localCustomer.getCustomerContacts());
        
        
        return customer;
    }

}












