package fit5042.ass.mbeans;
import java.io.IOException;
import java.io.Serializable;
import java.security.Principal;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.ass.repository.UserRepository;
import fit5042.ass.repository.entities.Users;

@Named(value = "authentication")
@SessionScoped
public class Authentication implements Serializable{

    private Users user; // The JPA entity.

    @EJB
    private UserRepository userRepository;

    public Users getUser() {
        if (user == null) {
            Principal principal = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
            if (principal != null) {
            	user = userRepository.find(principal.getName()); // Find User by j_username.
            }
        }
        return user;
    }
    
    public void logout() throws IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.invalidateSession();
        externalContext.redirect(externalContext.getRequestContextPath() + "/faces/index.xhtml");
    }

}
