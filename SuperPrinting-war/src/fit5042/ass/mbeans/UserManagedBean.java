package fit5042.ass.mbeans;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.xml.bind.DatatypeConverter;

import fit5042.ass.repository.UserRepository;
import fit5042.ass.repository.entities.Users;

/**
 * 
 * @author Yan
 *
 */

@Named(value = "userManagedBean")
@SessionScoped
public class UserManagedBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	UserRepository userRepository;
	private List<Integer> ids;
	
	@PostConstruct
	public void init(){
		try {
			ids = userRepository.getAllUserIds();
		} catch (Exception ex) {
            Logger.getLogger(UserManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
	}
	
	public UserRepository getUserRepository() {
		return userRepository;
	}

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public List<Integer> getIds() {
		return ids;
	}

	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}

	public UserManagedBean() {
	}
	
	public List<Users> getAllUsers() {
        try {
        	List<Users> users = userRepository.getAllUsers();
            return users;
        } catch (Exception ex) {
            Logger.getLogger(UserManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
	
	public List<Users> getAllNormalUsers() {
        try {
        	List<Users> users = userRepository.getAllNormalUsers();
            return users;
        } catch (Exception ex) {
            Logger.getLogger(UserManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
	
	public void addUser(fit5042.ass.userControllers.User localUser) throws Exception 
    {
		Users user = convertUserToEntity(localUser);
        try {
        	userRepository.addUser(user);
        } catch (Exception ex) {
            Logger.getLogger(UserManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
	
	private Users convertUserToEntity(fit5042.ass.userControllers.User localUser) throws Exception 
	{
		Users user = new Users(); //entity
    	
		user.setUsername(localUser.getUsername());
		user.setPassword(hashPassword(localUser.getPassword()));
		user.setGroupname(localUser.getGroupname());
               
        return user;
    }
	
	public String hashPassword(String pass) throws Exception {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(pass.getBytes(StandardCharsets.UTF_8));
		String encoded = DatatypeConverter.printHexBinary(hash); 
		return encoded;
	}
	
	
	public void editUser(Users user)
    {
        try {
        	user.setPassword(hashPassword(user.getPassword()));
        	userRepository.editUser(user);
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(UserManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
	
	public void removeUser(int userId) 
    {
        try {
        	userRepository.removeUser(userId);
        	
        } catch (Exception ex) {
            Logger.getLogger(UserManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
	
	public Users searchUserById(int userId)
    {
        try {
            return userRepository.searchUserById(userId);
        } catch (Exception ex) {
            Logger.getLogger(UserManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
	
	public List<Users> searchUserByName(String name) {
    	try {
            return userRepository.searchUserByName(name);
        } catch (Exception ex) {
            Logger.getLogger(UserManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
		return null;
	}
    
}
