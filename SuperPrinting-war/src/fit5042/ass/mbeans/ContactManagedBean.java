package fit5042.ass.mbeans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.ass.repository.ContactRepository;
import fit5042.ass.repository.entities.Contact;
import fit5042.ass.repository.entities.Customer;
import fit5042.ass.repository.entities.Users;

/**
* 
* @author Yan
*
*/

@Named(value = "contactManagedBean")
@SessionScoped
public class ContactManagedBean implements Serializable{
	
	@EJB
	ContactRepository contactRepository;

	public ContactManagedBean() {
	}
	
    public List<Contact> getAllContacts() {
        try {
        	List<Contact> contacts = contactRepository.getAllContacts();
            return contacts;
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void setContactList(List<Contact> contactList) {
    	try {
    		contactRepository.setContactList(contactList);
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
	}
    
    public void addContact(fit5042.ass.contactControllers.Contact localContact) throws Exception 
    {
    	Contact contact = convertContactToEntity(localContact);
        try {
        	contactRepository.addContact(contact);
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

	private Contact convertContactToEntity(fit5042.ass.contactControllers.Contact localContact) throws Exception 
	{
		Contact contact = new Contact(); //entity
    	
		contact.setName(localContact.getName());
		contact.setGender(localContact.getGender());
		contact.setTitle(localContact.getTitle());
		contact.setPosition(localContact.getPosition());
		contact.setPhoneNumber(localContact.getPhoneNumber());
		contact.setEmail(localContact.getEmail());
		contact.setCustomer(localContact.getCustomer());
		contact.setBirthdate(localContact.getBirthdate());
               
        return contact;
	}
	
	//private final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	
	//private Date parseDate(String date) throws Exception {
	//        return new Date(DATE_FORMAT.parse(date).getTime());
	//}

	public void editContact(Contact contact)
    {
        try {
        	contactRepository.editContact(contact);
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
	
	public void removeContact(int contactId) 
    {
        try {
        	contactRepository.removeContact(contactId);
        	
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
	
	public Contact searchContactById(int contactId)
    {
        try {
            return contactRepository.searchContactById(contactId);
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
	
	public List<Contact> searchContactByName(String name) {
    	try {
            return contactRepository.searchContactByName(name);
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
		return null;
	}
	
	public List<Contact> searchContactByCustomerId(int customerId)
    {
        try {
            return contactRepository.searchContactByCustomerId(customerId);
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
	
}
