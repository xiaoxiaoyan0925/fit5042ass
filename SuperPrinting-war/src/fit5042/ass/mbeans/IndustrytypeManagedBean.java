package fit5042.ass.mbeans;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.ass.repository.IndustrytypeRepository;
import fit5042.ass.repository.entities.Industrytype;
import fit5042.ass.repository.entities.Users;

/**
 * 
 * @author Yan
 *
 */

@Named(value = "industrytypeManagedBean")
@SessionScoped
public class IndustrytypeManagedBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	IndustrytypeRepository industrytypeRepository;
	private List<String> typenames;
	
	public IndustrytypeManagedBean()  {
	}
	
	@PostConstruct
	public void init(){
		try {
			typenames = industrytypeRepository.getAllIndustrytypeNames();
		} catch (Exception ex) {
            Logger.getLogger(IndustrytypeManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
	}
	
	public List<Industrytype> getAllIndustrytypes() {
        try {
        	List<Industrytype> industrytypes = industrytypeRepository.getAllIndustrytypes();
            return industrytypes;
        } catch (Exception ex) {
            Logger.getLogger(IndustrytypeManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

	public void addIndustrytype(fit5042.ass.industrytypeControllers.Industrytype localIndustrytype) 
    {
		Industrytype industrytype = convertIndustrytypeToEntity(localIndustrytype);
        try {
        	industrytypeRepository.addIndustrytype(industrytype);
        } catch (Exception ex) {
            Logger.getLogger(IndustrytypeManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

	private Industrytype convertIndustrytypeToEntity(fit5042.ass.industrytypeControllers.Industrytype localIndustrytype) 
	{
		Industrytype industrytype = new Industrytype(); //entity  	
		industrytype.setTypename(localIndustrytype.getTypename());           
        return industrytype;
	}
	
	public void editIndustrytype(Industrytype industrytype)
    {
        try {
        	industrytypeRepository.editIndustrytype(industrytype);
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Industrytype has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(IndustrytypeManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
	
	public void removeIndustrytype(int id) 
    {
        try {
        	industrytypeRepository.removeIndustrytype(id);
        	
        } catch (Exception ex) {
            Logger.getLogger(IndustrytypeManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
	
	public List<Industrytype> searchIndustrytypeByName(String name) 
	{
    	try {
            return industrytypeRepository.searchIndustrytypeByName(name);
        } catch (Exception ex) {
            Logger.getLogger(IndustrytypeManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
		return null;
	}

	public List<String> getTypenames() {
		return typenames;
	}

	public void setTypenames(List<String> typenames) {
		this.typenames = typenames;
	}
	
}
