package fit5042.ass.customerControllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 * 
 * @author Yan
 *
 */

@Named(value = "customerController2")
@RequestScoped
public class CustomerController2 {

	private int customerId;
	private fit5042.ass.repository.entities.Customer customer;
	
	public CustomerController2() {
			customerId = Integer.valueOf(FacesContext.getCurrentInstance()
	                .getExternalContext()
	                .getRequestParameterMap()
	                .get("customerID"));
		
        // Assign property based on the id provided 
        customer = getCustomer();
    }

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
		
	
	public void setCustomer(fit5042.ass.repository.entities.Customer customer) {
		this.customer = customer;
	}

	public fit5042.ass.repository.entities.Customer getCustomer() {
        if (customer == null) {
            ELContext context
                    = FacesContext.getCurrentInstance().getELContext();
            CustomerApp customerApp
                    = (CustomerApp) FacesContext.getCurrentInstance()
                            .getApplication()
                            .getELResolver()
                            .getValue(context, null, "customerApp");
            // -1 to customerId to have positive customer id
            return customerApp.getCustomers().get(--customerId); 
        }
        return customer;
    }
}