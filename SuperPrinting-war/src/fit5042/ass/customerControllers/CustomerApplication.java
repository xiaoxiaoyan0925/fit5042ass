package fit5042.ass.customerControllers;

import java.util.ArrayList;
import java.util.List;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import fit5042.ass.mbeans.CustomerManagedBean;
import fit5042.ass.repository.entities.Customer;
import fit5042.ass.repository.entities.DataCommunicationCustomer;
import fit5042.ass.repository.entities.HealthCustomer;

/**
 * 
 * @author Yan
 *
 */

@Named(value = "customerApplication")
@ApplicationScoped
public class CustomerApplication {
	
	@Inject
    CustomerManagedBean customerManagedBean;
	
	private List<Customer> customers;
	private List<DataCommunicationCustomer> dcCustomers;
	private List<HealthCustomer> heCustomers;

    private boolean showForm = true;


	public CustomerManagedBean getCustomerManagedBean() {
		return customerManagedBean;
	}

	public void setCustomerManagedBean(CustomerManagedBean customerManagedBean) {
		this.customerManagedBean = customerManagedBean;
	}

  
    public boolean isShowForm() {
        return showForm;
    }
    
    public CustomerApplication() throws Exception { 
    	//instantiate customer list
    	customers = new ArrayList<>();
    	dcCustomers = new ArrayList<>();
    	heCustomers = new ArrayList<>();
    	  
        //instantiate customerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
        .getELResolver().getValue(elContext, null, "customerManagedBean");
        
        //get customers from db 
        updateCustomerList();
    }
    
    

	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}
	
	public List<DataCommunicationCustomer> getDcCustomers() {
		return dcCustomers;
	}

	public void setDcCustomers(List<DataCommunicationCustomer> dcCustomers) {
		this.dcCustomers = dcCustomers;
	}

	public void updateCustomerList() {
		if (customers == null || customers.size() <= 0)
        {
        	customers.clear();
        	dcCustomers.clear();
        	heCustomers.clear();

            for (fit5042.ass.repository.entities.Customer customer : customerManagedBean.getAllCustomers())
            {
            	customers.add(customer);
            }
            
            for (fit5042.ass.repository.entities.DataCommunicationCustomer dcCustomer : customerManagedBean.getAllDcCustomers())
            {
            	dcCustomers.add(dcCustomer);
            }
            
            for (fit5042.ass.repository.entities.HealthCustomer heCustomer : customerManagedBean.getAllHeCustomers())
            {
            	heCustomers.add(heCustomer);
            }

            setCustomers(customers);
            setDcCustomers(dcCustomers);
            setHeCustomers(heCustomers);
        }
		
	}
	
	public void searchCustomerById(int customerId)
    {
		customers.clear();
		customers.add(customerManagedBean.searchCustomerById(customerId));
    }
	
	public void searchCustomerByUserid(int userId)
    {
		customers.clear();
		for (fit5042.ass.repository.entities.Customer customer : customerManagedBean.searchCustomerByUserid(userId))
        {
			customers.add(customer);
        };
        setCustomers(customers);
		
    }
	
	public void searchCustomerByName(String name) {
		customers.clear();
		for (fit5042.ass.repository.entities.Customer customer : customerManagedBean.searchCustomerByName(name))
        {
			customers.add(customer);
        }
        setCustomers(customers);
	}
	
	public void searchCustomerByMultipleField(String fiele1, String value1, String field2, String value2) {
		customers.clear();
		for (fit5042.ass.repository.entities.Customer customer : customerManagedBean.searchCustomerByMultipleField(fiele1, value1, field2, value2))
        {
			customers.add(customer);
        }
        setCustomers(customers);
	}
	
	public void searchAll()
    {
		customers.clear();
		dcCustomers.clear();
        
		for (fit5042.ass.repository.entities.Customer customer : customerManagedBean.getAllCustomers())
        {
        	customers.add(customer);
        }
        
		for (fit5042.ass.repository.entities.DataCommunicationCustomer dcCustomer : customerManagedBean.getAllDcCustomers())
        {
        	dcCustomers.add(dcCustomer);
        }

        setCustomers(customers);
        setDcCustomers(dcCustomers);
    }

	public List<HealthCustomer> getHeCustomers() {
		return heCustomers;
	}

	public void setHeCustomers(List<HealthCustomer> heCustomers) {
		this.heCustomers = heCustomers;
	}
}





















