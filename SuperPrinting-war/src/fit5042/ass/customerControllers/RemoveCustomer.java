package fit5042.ass.customerControllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import fit5042.ass.mbeans.CustomerManagedBean;

/**
 * 
 * @author Yan
 *
 */
@RequestScoped
@Named("removeCustomer")
public class RemoveCustomer {
	
	@Inject
    CustomerManagedBean customerManagedBean;


	public CustomerManagedBean getCustomerManagedBean() {
		return customerManagedBean;
	}

	public void setCustomerManagedBean(CustomerManagedBean customerManagedBean) {
		this.customerManagedBean = customerManagedBean;
	}

    
    private boolean showForm = true;
    
    private Customer customer;
    
    CustomerApplication app;
    
    public RemoveCustomer() 
    {
        ELContext elContext1 = FacesContext.getCurrentInstance().getELContext();
        app = (CustomerApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(elContext1, null, "customerApplication"); 
        app.updateCustomerList();
        
        //instantiate customerManagedBean
        ELContext elContext2 = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance()
        														.getApplication()
        														.getELResolver()
        														.getValue(elContext2, null, "customerManagedBean");
    }
    
    public boolean isShowForm() {
        return showForm;
    }

    

    public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}


	public void removeCustomer(int customerId) {
       try
       {
            //remove this property from db via EJB
    	   customerManagedBean.removeCustomer(customerId);

            //refresh the list in PropertyApplication bean
            app.searchAll();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been deleted succesfully")); 
                
       }
       catch (Exception ex)
       {
           
       }
       showForm = true;

    }
 
}
