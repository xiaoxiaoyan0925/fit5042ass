package fit5042.ass.customerControllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;


/**
*
* @author Yan
* 
*/

@RequestScoped
@Named("searchCustomer")
public class SearchCustomer {
   private Customer customer;
   
   CustomerApplication app;
   CustomerApp app2;
   
   private int searchById;
   private String searchByName;
   private int searchByUserid;
   private String field1;
   private String field2;
   private String value1;
   private String value2;
   
   public String getField1() {
	return field1;
	}
	
	public void setField1(String field1) {
		this.field1 = field1;
	}
	
	public String getField2() {
		return field2;
	}
	
	public void setField2(String field2) {
		this.field2 = field2;
	}
	
	public String getValue1() {
		return value1;
	}
	
	public void setValue1(String value1) {
		this.value1 = value1;
	}
	
	public String getValue2() {
		return value2;
	}
	
	public void setValue2(String value2) {
		this.value2 = value2;
	}

   
	public SearchCustomer() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (CustomerApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "customerApplication");
        app2 = (CustomerApp) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerApp");
        
        app.updateCustomerList();
        app2.updateCustomerList();
    }
   
   public Customer getCustomer() {
	   return customer;
   }

   public void setCustomer(Customer customer) {
	   this.customer = customer;
   }

   public CustomerApplication getApp() {
	   return app;
   }

   public void setApp(CustomerApplication app) {
	   this.app = app;
   }

   
	public CustomerApp getApp2() {
	return app2;
	}
	
	public void setApp2(CustomerApp app2) {
		this.app2 = app2;
	}

	public int getSearchById() {
		return searchById;
	}

	public void setSearchById(int searchById) {
		this.searchById = searchById;
	}

	public String getSearchByName() {
		return searchByName;
	}

	public void setSearchByName(String searchByName) {
		this.searchByName = searchByName;
	}
	
	public int getSearchByUserid() {
		return searchByUserid;
	}

	public void setSearchByUserid(int searchByUserid) {
		this.searchByUserid = searchByUserid;
	}

	public void searchCustomerById(int customerId) 
    {
       try
       {
            //search this customer then refresh the list in PropertyApplication bean
            app.searchCustomerById(customerId);
       }
       catch (Exception ex)
       {
           
       }
    }
    
    public void searchCustomerByName(String name)
    {
       try
       {
            //search this customer then refresh the list in CustomerApplication bean
            app.searchCustomerByName(name);
       }
       catch (Exception ex)
       {
           
       }
    }
    
    public void searchCustomerByUserid(int userid)
    {
       try
       {
            //search this customer then refresh the list in CustomerApplication bean
            app.searchCustomerByUserid(userid);
            app2.searchCustomerByUserid(userid);
       }
       catch (Exception ex)
       {
           
       }
    }
    
    public void searchCustomerByMultipleField(String field1, String value1, String field2, String value2) 
    {
       try
       {
            //search this customer then refresh the list in CustomerApplication bean
            app.searchCustomerByMultipleField(field1, value1, field2, value2);
       }
       catch (Exception ex)
       {
           
       }
    }
    
    public void searchAll() 
    {
       try
       {
             app.searchAll();
       }
       catch (Exception ex)
       {
           
       }
    }


	
}
