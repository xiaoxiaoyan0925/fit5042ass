package fit5042.ass.customerControllers;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * @author Yan
 */

@Named(value = "titleController")
@RequestScoped
public class TitleController {
	
	private String pageTitle;
	
	private TitleController(){
		setPageTitle("Super Printing");
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}	
	
}
