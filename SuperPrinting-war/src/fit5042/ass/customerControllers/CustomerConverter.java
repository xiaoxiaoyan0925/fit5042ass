package fit5042.ass.customerControllers;

import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import fit5042.ass.mbeans.CustomerManagedBean;
import fit5042.ass.repository.entities.Customer;

import java.util.List;

import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

/**
 * 
 * @author Yan
 *
 */

@FacesConverter(forClass = fit5042.ass.repository.entities.Customer.class, value = "customer")

public class CustomerConverter implements Converter {

	@Inject
    CustomerManagedBean customerManagedBean;

    public List<Customer> customerDB; 

    public CustomerConverter() {
        try {
            //instantiate propertyManagedBean
            ELContext elContext = FacesContext.getCurrentInstance().getELContext();
            customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
            						.getELResolver().getValue(elContext, null, "customerManagedBean");

            customerDB = customerManagedBean.getAllCustomers();
        } catch (Exception ex) {

        }
    }

    //this method is for converting the submitted value (as String) to the contact person object
    public Customer getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue.trim().equals("")) {
            return null;
        } else {
            try {
                int number = Integer.parseInt(submittedValue);

                for (Customer c : customerDB) {
                    if (c.getCustomerId() == number) {
                        return c;
                    }
                }

            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid customer"));
            }
        }
        return null;
    }

    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((Customer) value).getCustomerId());
        }
    }

}
