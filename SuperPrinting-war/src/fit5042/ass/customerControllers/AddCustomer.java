package fit5042.ass.customerControllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import fit5042.ass.mbeans.CustomerManagedBean;

/**
 * 
 * @author Yan
 *
 */
@RequestScoped
@Named("addCustomer")
public class AddCustomer {
	
	@Inject
    CustomerManagedBean customerManagedBean;
	
	private boolean showForm = true;

    private Customer customer;
    
    CustomerApplication app;
    CustomerApp app2;


	public CustomerManagedBean getCustomerManagedBean() {
		return customerManagedBean;
	}

	public void setCustomerManagedBean(CustomerManagedBean customerManagedBean) {
		this.customerManagedBean = customerManagedBean;
	}
	
    
    public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public AddCustomer() 
    {
    	ELContext elContext1 = FacesContext.getCurrentInstance().getELContext();
        app = (CustomerApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(elContext1, null, "customerApplication"); 
        app.updateCustomerList();
        app2 = (CustomerApp) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(elContext1, null, "customerApp"); 
        app2.updateCustomerList();
        
        //instantiate customerManagedBean
        ELContext elContext2 = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance()
        														.getApplication()
        														.getELResolver()
        														.getValue(elContext2, null, "customerManagedBean");
    }

    public boolean isShowForm() {
        return showForm;
    }

    public void addCustomer(Customer localCustomer) {
        //this is the local property, not the entity
       try
       {
            //add this customer to db via EJB
    	   customerManagedBean.addCustomer(localCustomer);

            //refresh the list in CustomerApplication bean
            app.searchAll();
            
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been added succesfully"));
       }
       catch (Exception ex)
       {
           
       }
        showForm = true;
    }
   
}


