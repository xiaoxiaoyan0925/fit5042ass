package fit5042.ass.customerControllers;

import java.util.ArrayList;
import java.util.List;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import fit5042.ass.mbeans.CustomerManagedBean;
import fit5042.ass.repository.entities.Customer;

/**
 * 
 * @author Yan
 *
 */

@Named(value = "customerApp")
@ApplicationScoped
public class CustomerApp {
	
	@Inject
    CustomerManagedBean customerManagedBean;
	
	private List<Customer> customers;

    private boolean showForm = true;


	public CustomerManagedBean getCustomerManagedBean() {
		return customerManagedBean;
	}

	public void setCustomerManagedBean(CustomerManagedBean customerManagedBean) {
		this.customerManagedBean = customerManagedBean;
	}

  
    public boolean isShowForm() {
        return showForm;
    }
    
    public CustomerApp() throws Exception { 
    	//instantiate customer list
    	customers = new ArrayList<>();
        
        //instantiate customerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
        .getELResolver().getValue(elContext, null, "customerManagedBean");
        updateCustomerList();
    }
    
    

	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}
	
	public void updateCustomerList() {
		if (customers == null || customers.size() <= 0)
        {
        	customers.clear();
            setCustomers(customers);
        }
		
	}
	
	public void searchCustomerByUserid(int userId)
    {
		customers.clear();
		for (fit5042.ass.repository.entities.Customer customer : customerManagedBean.searchCustomerByUserid(userId))
        {
			customers.add(customer);
        };
        setCustomers(customers);
		
    }

}
