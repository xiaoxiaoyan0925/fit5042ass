package fit5042.ass.customerControllers;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import fit5042.ass.repository.entities.Address;
/**
 * 
 * @author Yan
 *
 */

@RequestScoped
@Named(value = "customer")
public class Customer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int customerId;
	private String name;
	private String phoneNumber;
	private String email;
	private String fax;
	private String industryType;
	private String description;
	private Address address;
	private int userid;
	
	private String streetNumber;
    private String streetAddress;
    private String suburb;
    private String postcode;
    private String state;

	private List<fit5042.ass.repository.entities.Customer> customers;
	private List<fit5042.ass.repository.entities.Contact> contacts;
	
	public Customer() {
    }

	public Customer(int customerId, String name, String phoneNumber, String email, String fax, String industryType, String description, 
				Address address, int userid, List<fit5042.ass.repository.entities.Contact> contacts) 
	{
		super();
		this.customerId = customerId;
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.fax = fax;
		this.industryType = industryType;
		this.description = description;
		this.address = address;
		this.userid = userid;
		this.contacts = contacts;
	}
	
	// 
	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getSuburb() {
		return suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<fit5042.ass.repository.entities.Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<fit5042.ass.repository.entities.Customer> customers) {
		this.customers = customers;
	}

	//getters and setters of attributes of customers
	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	//toString method
	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", name=" + name + ", phoneNumber=" + phoneNumber + ", email="
				+ email + ", fax=" + fax + ", industryType=" + industryType + ", description=" + description
				+ ", address=" + address + ", userid=" + userid + ", customers="
				+ customers + "]";
	}

	public List<fit5042.ass.repository.entities.Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<fit5042.ass.repository.entities.Contact> contacts) {
		this.contacts = contacts;
	}


	
	

	
}















