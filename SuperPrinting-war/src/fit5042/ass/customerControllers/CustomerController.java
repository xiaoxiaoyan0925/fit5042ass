package fit5042.ass.customerControllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 * 
 * @author Yan
 *
 */

@Named(value = "customerController")
@RequestScoped
public class CustomerController {

	private int customerId;
	private fit5042.ass.repository.entities.Customer customer;
	
	public CustomerController() {
			customerId = Integer.valueOf(FacesContext.getCurrentInstance()
	                .getExternalContext()
	                .getRequestParameterMap()
	                .get("customerID"));
		
        // Assign property based on the id provided 
        customer = getCustomer();
    }

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
		
	
	public void setCustomer(fit5042.ass.repository.entities.Customer customer) {
		this.customer = customer;
	}

	public fit5042.ass.repository.entities.Customer getCustomer() {
        if (customer == null) {
            ELContext context
                    = FacesContext.getCurrentInstance().getELContext();
            CustomerApplication customerApplication
                    = (CustomerApplication) FacesContext.getCurrentInstance()
                            .getApplication()
                            .getELResolver()
                            .getValue(context, null, "customerApplication");
            // -1 to customerId to have positive customer id
            return customerApplication.getCustomers().get(--customerId); 
        }
        return customer;
    }
}
