package fit5042.ass.webServices;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.DatatypeConverter;
import javax.ws.rs.core.MediaType;

import fit5042.ass.repository.CustomerRepository;
import fit5042.ass.repository.UserRepository;
import fit5042.ass.repository.entities.Customer;
import fit5042.ass.repository.entities.Users;

@Path("getCustomer")
public class WebServices2 {
    @SuppressWarnings("unused")
    @Context
    private UriInfo context;

    @EJB
    UserRepository userRepository;
    
    @EJB
    private IdStorageBean idStorage;
    /**
     * Default constructor. 
     */
    public WebServices2() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Retrieves representation of an instance of WebServices
     * @return an instance of String
     * @throws Exception 
     * @throws NumberFormatException 
     */
    
	@GET
	@Path("byId")
	@Produces(MediaType.APPLICATION_JSON)
	public Users getCustomer() throws Exception {
		return userRepository.searchUserById(Integer.parseInt(idStorage.getId()));
	}
	
	@GET
	@Path("all")
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_ATOM_XML})
	public List<Users> getUsers() throws Exception {
		return userRepository.getAllNormalUsers();
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	public void setPostName(@FormParam("id") String id) {
		idStorage.setId(id);
	}


}
