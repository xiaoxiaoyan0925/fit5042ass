package fit5042.ass.webServices;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;

/**
 * Session Bean implementation class NameStorageBean
 */
@Singleton
@LocalBean
public class IdStorageBean {
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	

}
