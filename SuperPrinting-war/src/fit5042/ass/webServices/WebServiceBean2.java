package fit5042.ass.webServices;

import java.io.Serializable;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.ass.mbeans.UserManagedBean;
import fit5042.ass.webServices.*;

@Named(value = "webServiceBean")
@SessionScoped
public class WebServiceBean2 implements Serializable {
	
	private String name;
	private String id;
	private WebServiceClient webServiceClient;

	public WebServiceBean2() {
	}

	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public void setWebServiceClient() {
		webServiceClient = new WebServiceClient();
		webServiceClient.setPostId2(getName());
		webServiceClient.setPostId3(getId());
	}
}
