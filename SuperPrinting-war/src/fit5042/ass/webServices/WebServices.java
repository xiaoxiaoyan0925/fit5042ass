package fit5042.ass.webServices;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.DatatypeConverter;

import fit5042.ass.repository.CustomerRepository;
import fit5042.ass.repository.entities.Customer;

@Path("greeting")
public class WebServices {
    @SuppressWarnings("unused")
    @Context
    private UriInfo context;

    @EJB
    private NameStorageBackenBean nameStorage;
    /**
     * Default constructor. 
     */
    public WebServices() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Retrieves representation of an instance of WebServices
     * @return an instance of String
     * @throws Exception 
     * @throws NumberFormatException 
     */
    
	@GET
	@Produces("text/html")
	public String getHtml() throws Exception {
		return "<html><h:head align=\"center\">\r\n" + 
				"		<link rel=\"stylesheet\" href=\"https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css\"/>  \r\n" + 
				"		<style>\r\n" + 
				"			ul {\r\n" + 
				"			  list-style-type: none;\r\n" + 
				"			  margin: 0;\r\n" + 
				"			  padding: 0;\r\n" + 
				"			  overflow: hidden;\r\n" + 
				"			  background-color: #006652;\r\n" + 
				"			}\r\n" + 
				"			\r\n" + 
				"			li {\r\n" + 
				"			  float: left;\r\n" + 
				"			}\r\n" + 
				"			\r\n" + 
				"			li a {\r\n" + 
				"			  display: block;\r\n" + 
				"			  color: white;\r\n" + 
				"			  text-align: center;\r\n" + 
				"			  font-weight:bold;\r\n" + 
				"			  font-size:medium;\r\n" + 
				"			  padding: 14px 16px;\r\n" + 
				"			  text-decoration: none;\r\n" + 
				"			}\r\n" + 
				"			\r\n" + 
				"			li a:hover {\r\n" + 
				"			  background-color: #99ffeb;\r\n" + 
				"			}\r\n" + 
				"			h:graphicImage{\r\n" + 
				"				width:150px;\r\n" + 
				"				height:50px;\r\n" + 
				"			}\r\n" + 
				"			body{\r\n" + 
				"				background-color: #e6fffa;\r\n" + 
				"				font-family: \"Lucida Console\", Courier, monospace;\r\n" + 
				"			}\r\n" + 
				"			tr{\r\n" + 
				"			font-size:medium;\r\n" + 
				"			color: black;\r\n" + 
				"			}\r\n" + 
				"		</style>\r\n" + 
				"	</h:head> \r\n" + 
				"	<body> \r\n" + 
				"	\r\n" + 
				"		<ul>\r\n" + 
				"		  <li> &nbsp;<h:graphicImage library=\"images\" name=\"logo.svg\" id=\"logo\"  width=\"150\" height=\"50\"/> &nbsp;</li>\r\n" + 
				"		  <li><a class=\"active\" href=\"faces/index.xhtml\">Home</a></li>\r\n" + 
				"		  <li><a href=\"#news\">News</a></li>\r\n" + 
				"		  <li><a href=\"#contact\">Contact</a></li>\r\n" + 
				"		  <li><a href=\"#about\">About</a></li>\r\n" + 
				"		  <li style=\"float:right\"><h:form>\r\n" + 
				"					<h:commandLink value=\"^-^LogOut\" action=\"#{authentication.logout}\"></h:commandLink>\r\n" + 
				"                 </h:form></li>\r\n" + 
				"          <li style=\"float:right\"><a href=\"faces/loginRole.xhtml\" class=\"active\"><span class=\"glyphicon glyphicon-log-in\"></span> LogIn</a></li>\r\n" + 
				"		</ul>\r\n" + 
				"	<br/><h5>&nbsp;&nbsp;&nbsp;&nbsp;Your String is:  " + nameStorage.getName() + "</h5><h5>&nbsp;&nbsp;&nbsp;&nbsp;The encoded password is:  " + hashPassword(nameStorage.getName()) + "</h5></body></html>";
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	public void setPostName(@FormParam("name") String content) {
		nameStorage.setName(content);
	}
	
	public String hashPassword(String pass) throws Exception {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(pass.getBytes(StandardCharsets.UTF_8));
		String encoded = DatatypeConverter.printHexBinary(hash); 
		return encoded;
	}

}