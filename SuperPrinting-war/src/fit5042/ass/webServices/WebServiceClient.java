package fit5042.ass.webServices;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

public class WebServiceClient {
	private WebTarget webTarget;
	private WebTarget webTarget2;
    private Client client;
    private static final String BASE_URI = "http://localhost:8080/SuperPrinting-war/webresources";

    public WebServiceClient() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("greeting");
        webTarget2 = client.target(BASE_URI).path("getCustomer");
    }

    public String getCustomerObject() throws ClientErrorException {
        WebTarget resource = webTarget2;
        return resource.request(javax.ws.rs.core.MediaType.TEXT_HTML).get(String.class);
    }
    
    public void setPostId3(String id) throws ClientErrorException {
        Form form = new Form();
        form.param("id", id);
        webTarget2.request(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE));
    }

    public void setPostId() throws ClientErrorException {
        webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED).post(null);
    }
    
    public void setPostId2(String id) throws ClientErrorException {
        Form form = new Form();
        form.param("name", id);
        webTarget.request(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE));
    }
    
    public void putHtml(Object requestEntity) throws ClientErrorException {
        webTarget.request(javax.ws.rs.core.MediaType.TEXT_HTML).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.TEXT_HTML));
    }
    
    public void close() {
        client.close();
    }
}
