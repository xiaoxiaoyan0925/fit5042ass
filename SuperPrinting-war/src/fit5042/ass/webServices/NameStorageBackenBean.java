package fit5042.ass.webServices;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;

/**
 * Session Bean implementation class NameStorageBackenBean
 */
@Singleton
@LocalBean
public class NameStorageBackenBean {

    /**
     * Default constructor. 
     */
    public NameStorageBackenBean() {
        // TODO Auto-generated constructor stub
    }
    
    private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    
	

}
