package fit5042.ass.contactControllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import fit5042.ass.mbeans.ContactManagedBean;

/**
 * 
 * @author Yan
 *
 */
@RequestScoped
@Named("removeContact")
public class RemoveContact {
	
	@Inject
	ContactManagedBean contactManagedBean;	
	private boolean showForm = true;
	private Contact contact;   
	ContactApplication app;
	
	
	public ContactManagedBean getContactManagedBean() {
		return contactManagedBean;
	}
	public void setContactManagedBean(ContactManagedBean contactManagedBean) {
		this.contactManagedBean = contactManagedBean;
	}
	public boolean isShowForm() {
		return showForm;
	}
	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}
	public Contact getContact() {
		return contact;
	}
	public void setContact(Contact contact) {
		this.contact = contact;
	}
	public ContactApplication getApp() {
		return app;
	}
	public void setApp(ContactApplication app) {
		this.app = app;
	}
	
	public RemoveContact() 
    {
    	ELContext elContext1 = FacesContext.getCurrentInstance().getELContext();
        app = (ContactApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(elContext1, null, "contactApplication"); 
        app.updateContactList();
        
        //instantiate contactManagedBean
        ELContext elContext2 = FacesContext.getCurrentInstance().getELContext();
        contactManagedBean = (ContactManagedBean) FacesContext.getCurrentInstance()
        														.getApplication()
        														.getELResolver()
        														.getValue(elContext2, null, "contactManagedBean");
    }
	
	public void removeContact(int id) {
        //this is the local Contact
       try
       {
            //add this contact to db via EJB
    	   contactManagedBean.removeContact(id);

            //refresh the list in ContactApplication bean
            app.searchAll();
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact has been deleted succesfully"));
       }
       catch (Exception ex)
       {
           
       }
        showForm = true;
    }

}
