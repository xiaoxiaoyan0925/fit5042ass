package fit5042.ass.contactControllers;

import java.util.ArrayList;
import java.util.List;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import fit5042.ass.mbeans.ContactManagedBean;
import fit5042.ass.repository.entities.Contact;

/**
 * 
 * @author Yan
 *
 */

@Named(value = "contactApplication")
@ApplicationScoped
public class ContactApplication {
	
	@Inject
	ContactManagedBean contactManagedBean;	
	private List<Contact> contacts;
    private boolean showForm = true;

    public ContactApplication() throws Exception { 
    	//instantiate customer list
    	contacts = new ArrayList<>();
        
        //instantiate customerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        contactManagedBean = (ContactManagedBean) FacesContext.getCurrentInstance().getApplication()
        .getELResolver().getValue(elContext, null, "contactManagedBean");
        
        //get customers from db 
        updateContactList();
    }


	public ContactManagedBean getContactManagedBean() {
		return contactManagedBean;
	}

	public void setContactManagedBean(ContactManagedBean contactManagedBean) {
		this.contactManagedBean = contactManagedBean;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public boolean isShowForm() {
		return showForm;
	}

	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}

	public void updateContactList() {
		if (contacts == null || contacts.size() <= 0)
        {
			contacts.clear();

            for (fit5042.ass.repository.entities.Contact contact : contactManagedBean.getAllContacts())
            {
            	contacts.add(contact);
            }
            setContacts(contacts);
        }
		
	}
	
	public void searchContactById(int id)
    {
		contacts.clear();
		contacts.add(contactManagedBean.searchContactById(id));
    }
	
	public void searchContactByName(String name) 
	{
		contacts.clear();
		for (fit5042.ass.repository.entities.Contact contact : contactManagedBean.searchContactByName(name))
        {
			contacts.add(contact);
        }
		setContacts(contacts);
	}
	
	public void searchContactByCustomerId(int id)
	{
		contacts.clear();
		for (fit5042.ass.repository.entities.Contact contact : contactManagedBean.searchContactByCustomerId(id))
        {
			contacts.add(contact);
        }
		setContacts(contacts);
	}
	
	public void searchAll()
    {
		contacts.clear();   
		for (fit5042.ass.repository.entities.Contact contact : contactManagedBean.getAllContacts())
		{
			contacts.add(contact);
        }
		setContacts(contacts);
    }
	

}
