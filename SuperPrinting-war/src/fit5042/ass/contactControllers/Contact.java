package fit5042.ass.contactControllers;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
/**
 * 
 * @author Yan
 *
 */

@RequestScoped
@Named(value = "contact")
public class Contact implements Serializable{
	
	private int contactId;
	private String name;
	private String gender;
	private String title;
	private String position;
	private Date birthdate;
	private String phoneNumber;
	private String email;
	
	private fit5042.ass.repository.entities.Customer customer;
	
	private List<fit5042.ass.repository.entities.Contact> contacts;
	
	public Contact() {
    }

	public Contact(int contactId, String name, String gender, String title, String position, String phoneNumber,Date birthdate,
			String email, fit5042.ass.repository.entities.Customer customer, List<fit5042.ass.repository.entities.Contact> contacts) {
		super();
		this.contactId = contactId;
		this.name = name;
		this.gender = gender;
		this.title = title;
		this.position = position;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.birthdate = birthdate;
		this.customer = customer;
		this.contacts = contacts;
	}

	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}



	public fit5042.ass.repository.entities.Customer getCustomer() {
		return customer;
	}

	public void setCustomer(fit5042.ass.repository.entities.Customer customer) {
		this.customer = customer;
	}

	public List<fit5042.ass.repository.entities.Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<fit5042.ass.repository.entities.Contact> contacts) {
		this.contacts = contacts;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	
	

}
