package fit5042.ass.contactControllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.ass.userControllers.User;
import fit5042.ass.userControllers.UserApplication;

/**
*
* @author Yan
* 
*/

@RequestScoped
@Named("searchContact")
public class SearchContact {
	
	private Contact contact;	   
	ContactApplication app;   
	private int searchById;
	private String searchByName;
	private int searchByCustomerId;
	
	public SearchContact() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (ContactApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "contactApplication");
        
        app.updateContactList();
    }

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public ContactApplication getApp() {
		return app;
	}

	public void setApp(ContactApplication app) {
		this.app = app;
	}

	public int getSearchById() {
		return searchById;
	}

	public void setSearchById(int searchById) {
		this.searchById = searchById;
	}

	public String getSearchByName() {
		return searchByName;
	}

	public void setSearchByName(String searchByName) {
		this.searchByName = searchByName;
	}

	public int getSearchByCustomerId() {
		return searchByCustomerId;
	}

	public void setSearchByCustomerId(int searchByCustomerId) {
		this.searchByCustomerId = searchByCustomerId;
	}
	
	public void searchContactById(int id) 
    {
       try
       {
            app.searchContactById(id);
       }
       catch (Exception ex)
       {
           
       }
    }
	
	public void searchContactByName(String name)
    {
       try
       {
            app.searchContactByName(name);
       }
       catch (Exception ex)
       {
           
       }
    }
	
	public void searchContactByCustomerId(int id)
    {
       try
       {
            app.searchContactByCustomerId(id);
       }
       catch (Exception ex)
       {
           
       }
    }
	
	
	public void searchAll() 
    {
       try
       {
             app.searchAll();
       }
       catch (Exception ex)
       {
           
       }
    }

}
