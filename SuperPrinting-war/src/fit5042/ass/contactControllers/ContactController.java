package fit5042.ass.contactControllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.ass.customerControllers.CustomerApplication;

/**
 * 
 * @author Yan
 *
 */

@Named(value = "contactController")
@RequestScoped
public class ContactController {

	private int contactId;
	private fit5042.ass.repository.entities.Contact contact;
	
	public ContactController() {
		contactId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("contactID"));
        // Assign property based on the id provided 
		contact = getContact();
    }

	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public fit5042.ass.repository.entities.Contact getContact() {
        if (contact == null) {
            ELContext context
                    = FacesContext.getCurrentInstance().getELContext();
            ContactApplication contactApplication
                    = (ContactApplication) FacesContext.getCurrentInstance()
                            .getApplication()
                            .getELResolver()
                            .getValue(context, null, "contactApplication");
            // -1 to contactId to have positive contact id
            return contactApplication.getContacts().get(--contactId); 
        }
        return contact;
	}
	

	
	

}
