package fit5042.ass.industrytypeControllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;


/**
 * 
 * @author Yan
 *
 */

@Named(value = "industrytypeController")
@RequestScoped
public class IndustrytypeController {

	private int id;
	private fit5042.ass.repository.entities.Industrytype industrytype;
	
	public IndustrytypeController() {
		setId(Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("industrytypeID")));
        // Assign property based on the id provided 
		industrytype = getIndustrytype();
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public fit5042.ass.repository.entities.Industrytype getIndustrytype() {
        if (industrytype == null) {
            ELContext context
                    = FacesContext.getCurrentInstance().getELContext();
            IndustrytypeApplication industrytypeApplication
                    = (IndustrytypeApplication) FacesContext.getCurrentInstance()
                            .getApplication()
                            .getELResolver()
                            .getValue(context, null, "industrytypeApplication");
            // -1 to industrytypeId to have positive industrytype id
            return industrytypeApplication.getIndustrytypes().get(--id); 
        }
        return industrytype;
    }

}
