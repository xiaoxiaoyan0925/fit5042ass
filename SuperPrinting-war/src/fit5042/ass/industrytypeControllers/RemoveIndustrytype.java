package fit5042.ass.industrytypeControllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import fit5042.ass.mbeans.IndustrytypeManagedBean;



/**
 * 
 * @author Yan
 *
 */
@RequestScoped
@Named("removeIndustrytype")
public class RemoveIndustrytype {

	@Inject
	IndustrytypeManagedBean industrytypeManagedBean;	
	private boolean showForm = true;
	private Industrytype industrytype;   
	IndustrytypeApplication app;
	
	public RemoveIndustrytype() 
    {
    	ELContext elContext1 = FacesContext.getCurrentInstance().getELContext();
        app = (IndustrytypeApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(elContext1, null, "industrytypeApplication"); 
        app.updateIndustrytypeList();
        
        //instantiate industrytypeManagedBean
        ELContext elContext2 = FacesContext.getCurrentInstance().getELContext();
        industrytypeManagedBean = (IndustrytypeManagedBean) FacesContext.getCurrentInstance()
        														.getApplication()
        														.getELResolver()
        														.getValue(elContext2, null, "industrytypeManagedBean");
    }

	public IndustrytypeManagedBean getIndustrytypeManagedBean() {
		return industrytypeManagedBean;
	}

	public void setIndustrytypeManagedBean(IndustrytypeManagedBean industrytypeManagedBean) {
		this.industrytypeManagedBean = industrytypeManagedBean;
	}

	public boolean isShowForm() {
		return showForm;
	}

	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}

	public Industrytype getIndustrytype() {
		return industrytype;
	}

	public void setIndustrytype(Industrytype industrytype) {
		this.industrytype = industrytype;
	}

	public IndustrytypeApplication getApp() {
		return app;
	}

	public void setApp(IndustrytypeApplication app) {
		this.app = app;
	}
	
	public void removeIndustrytype(int id) {
        //this is the local user
       try
       {
            //add this user to db via EJB
    	   industrytypeManagedBean.removeIndustrytype(id);

            //refresh the list in IndustrytypeApplication bean
            app.searchAll();
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Industrytype has been deleted succesfully"));
       }
       catch (Exception ex)
       {
           
       }
        showForm = true;
    }
}
