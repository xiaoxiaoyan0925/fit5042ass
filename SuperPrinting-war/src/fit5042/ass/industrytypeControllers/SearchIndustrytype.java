package fit5042.ass.industrytypeControllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.ass.userControllers.UserApplication;


/**
*
* @author Yan
* 
*/

@RequestScoped
@Named("searchIndustrytype")
public class SearchIndustrytype {
	
	private Industrytype industrytype;	   
	IndustrytypeApplication app;   
	private String searchByName;
	
	public SearchIndustrytype() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (IndustrytypeApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "industrytypeApplication");
        
        app.updateIndustrytypeList();
    }

	public Industrytype getIndustrytype() {
		return industrytype;
	}

	public void setIndustrytype(Industrytype industrytype) {
		this.industrytype = industrytype;
	}

	public IndustrytypeApplication getApp() {
		return app;
	}

	public void setApp(IndustrytypeApplication app) {
		this.app = app;
	}

	public String getSearchByName() {
		return searchByName;
	}

	public void setSearchByName(String searchByName) {
		this.searchByName = searchByName;
	}

	public void searchIndustrytypeByName(String name)
    {
       try
       {
            //search this industrytype then refresh the list in IndustrytypeApplication bean
            app.searchIndustrytypeByName(name);
       }
       catch (Exception ex)
       {
           
       }
    }
	
	public void searchAll() 
    {
       try
       {
             app.searchAll();
       }
       catch (Exception ex)
       {
           
       }
    }
}
