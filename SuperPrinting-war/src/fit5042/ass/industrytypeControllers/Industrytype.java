package fit5042.ass.industrytypeControllers;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * 
 * @author Yan
 *
 */

@RequestScoped
@Named(value = "industrytype")
public class Industrytype implements Serializable{


	private int id;
	private String typename;
	
	public Industrytype() {
	}

	public Industrytype(int id, String typename) {
		super();
		this.id = id;
		this.typename = typename;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTypename() {
		return typename;
	}

	public void setTypename(String typename) {
		this.typename = typename;
	}

	@Override
	public String toString() {
		return "Industrytype [id=" + id + ", typename=" + typename + "]";
	}
	
	
}
