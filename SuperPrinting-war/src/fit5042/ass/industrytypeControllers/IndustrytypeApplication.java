package fit5042.ass.industrytypeControllers;

import java.util.ArrayList;
import java.util.List;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import fit5042.ass.mbeans.IndustrytypeManagedBean;
import fit5042.ass.repository.entities.Industrytype;


/**
 * 
 * @author Yan
 *
 */

@Named(value = "industrytypeApplication")
@ApplicationScoped
public class IndustrytypeApplication {

	@Inject
	IndustrytypeManagedBean industrytypeManagedBean;
	private List<Industrytype> industrytypes;
    private boolean showForm = true;
    
    public IndustrytypeApplication() throws Exception { 
    	//instantiate user list
    	industrytypes = new ArrayList<>();
        
        //instantiate industrytypeManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        industrytypeManagedBean = (IndustrytypeManagedBean) FacesContext.getCurrentInstance().getApplication()
        				.getELResolver().getValue(elContext, null, "industrytypeManagedBean");
        
        //get users from db 
        updateIndustrytypeList();
        
    }

	public void updateIndustrytypeList() {
		if (industrytypes == null || industrytypes.size() <= 0)
        {
			industrytypes.clear();

            for (fit5042.ass.repository.entities.Industrytype industrytype : industrytypeManagedBean.getAllIndustrytypes())
            {
            	industrytypes.add(industrytype);
            }
            setIndustrytypes(industrytypes);
        }
		
	}

	public IndustrytypeManagedBean getIndustrytypeManagedBean() {
		return industrytypeManagedBean;
	}

	public void setIndustrytypeManagedBean(IndustrytypeManagedBean industrytypeManagedBean) {
		this.industrytypeManagedBean = industrytypeManagedBean;
	}

	public List<Industrytype> getIndustrytypes() {
		return industrytypes;
	}

	public void setIndustrytypes(List<Industrytype> industrytypes) {
		this.industrytypes = industrytypes;
	}

	public boolean isShowForm() {
		return showForm;
	}

	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}
	
	public void searchIndustrytypeByName(String name) {
		industrytypes.clear();
		for (fit5042.ass.repository.entities.Industrytype industrytype : industrytypeManagedBean.searchIndustrytypeByName(name))
        {
			industrytypes.add(industrytype);
        }
        setIndustrytypes(industrytypes);
	}
	
	public void searchAll()
    {
		industrytypes.clear();    
		for (fit5042.ass.repository.entities.Industrytype industrytype : industrytypeManagedBean.getAllIndustrytypes())
        {
			industrytypes.add(industrytype);
        }
		setIndustrytypes(industrytypes);
    }
    
    
}
