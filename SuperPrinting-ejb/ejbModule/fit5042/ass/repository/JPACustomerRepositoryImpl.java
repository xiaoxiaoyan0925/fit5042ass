package fit5042.ass.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import fit5042.ass.repository.entities.*;

/**
 * 
 * @author Yan
 *
 */

@Stateless
public class JPACustomerRepositoryImpl implements CustomerRepository{

	@PersistenceContext(unitName = "Ass-ejbPU")
    private EntityManager entityManager;
	
	
	@Override
	public List<Customer> getAllCustomers() throws Exception {
		return entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
    }
	
	@Override
	public List<DataCommunicationCustomer> getAllDcCustomers() throws Exception{
		return entityManager.createNamedQuery(DataCommunicationCustomer.GET_ALL_QUERY_NAME).getResultList();
	}
	
	@Override
	public List<HealthCustomer> getAllHeCustomers() throws Exception{
		return entityManager.createNamedQuery(HealthCustomer.GET_ALL_QUERY_NAME).getResultList();
	}

	@Override
	public void setCustomerList(List<Customer> customerList) {
		// TODO Auto-generated method stub  ...
		
	}


	@Override
	public void addCustomer(Customer customer) throws Exception {
    	List<Customer> customers =  entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList(); 
    	customer.setCustomerId(customers.get(0).getCustomerId() + 1);
        entityManager.persist(customer);
    }

	@Override
	public void editCustomer(Customer customer) throws Exception {   
        entityManager.merge(customer);

    }
	
	@Override
	public void removeCustomer(int customerId) throws Exception {
		Customer customer = entityManager.find(Customer.class, customerId);
        entityManager.remove(customer);
    }

	@Override
	public int getCustomerId() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Customer searchCustomerById(int customerId) throws Exception {
		Customer customer = entityManager.find(Customer.class, customerId);
        return customer;
    }

	@Override
	public List<Customer> searchCustomerByUserid(int userid) throws Exception {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery query = builder.createQuery(Customer.class);
        Root<Customer> c = query.from(Customer.class);
        query.select(c).where(builder.equal(c.get("userid"), userid));
        List<Customer> customers = entityManager.createQuery(query).getResultList();
        return customers;
    }
	
	@Override
	public List<Contact> getAllCustomerContacts() throws Exception {
        return entityManager.createNamedQuery(Contact.GET_ALL_QUERY_NAME).getResultList();
    }

	@Override
	public List<Customer> searchCustomerByName(String name) throws Exception {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery query = builder.createQuery(Customer.class);
        Root<Customer> c = query.from(Customer.class);
        query.select(c).where(builder.like(c.get("name"), ("%"+name+"%")));
        List<Customer> customers = entityManager.createQuery(query).getResultList();
        return customers;
        
    }

	@Override
	public List<Customer> searchCustomerByMultipleField(String field1, String value1, String field2, String value2) throws Exception {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Customer> criteriaQuery = criteriaBuilder.createQuery(Customer.class);
		Root<Customer> c = criteriaQuery.from(Customer.class);
		Predicate predicateForField1
		  = criteriaBuilder.like(c.get(field1), ("%"+value1+"%"));
		Predicate predicateForField2
		  = criteriaBuilder.like(c.get(field2), ("%"+value2+"%"));
		Predicate predicateForBoth
		  = criteriaBuilder.and(predicateForField1, predicateForField2);
		criteriaQuery.where(predicateForBoth);
		List<Customer> customers = entityManager.createQuery(criteriaQuery).getResultList();
        return customers;
        
    }


}
