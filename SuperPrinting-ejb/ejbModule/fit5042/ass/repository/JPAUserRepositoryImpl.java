package fit5042.ass.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import fit5042.ass.repository.entities.Users;


/**
 * 
 * @author Yan
 *
 */

@Stateless
public class JPAUserRepositoryImpl implements UserRepository{

	@PersistenceContext(unitName = "Ass-ejbPU")
    private EntityManager entityManager;
	
	@Override
	public List<Users> getAllUsers() throws Exception {
		return entityManager.createNamedQuery(Users.GET_ALL_QUERY_NAME).getResultList();
	}


	@Override
	public List<Users> getAllNormalUsers() throws Exception {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Users> query = builder.createQuery(Users.class);
        Root<Users> u = query.from(Users.class);
        query.select(u).where(builder.like(u.get("groupname"), "normalUser"));
        List<Users> users = entityManager.createQuery(query).getResultList();
        return users;
	}
	
	@Override
	public void setUserList(List<Users> userList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeUser(int userId) throws Exception {
		Users user = entityManager.find(Users.class, userId);
        entityManager.remove(user);	
	}

	@Override
	public void addUser(Users user) throws Exception {
		List<Users> users =  entityManager.createNamedQuery(Users.GET_ALL_QUERY_NAME).getResultList(); 
		user.setId(users.get(0).getId() + 1);
        entityManager.persist(user);
		
	}

	@Override
	public void editUser(Users user) throws Exception {
		entityManager.merge(user);
		
	}

	@Override
	public int getUserId() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Users searchUserById(int userId) throws Exception {
		Users user = entityManager.find(Users.class, userId);
        return user;
	}

	@Override
	public List<Users> searchUserByName(String name) throws Exception {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Users> query = builder.createQuery(Users.class);
        Root<Users> u = query.from(Users.class);
        query.select(u).where(builder.like(u.get("username"), ("%"+name+"%")));
        List<Users> users = entityManager.createQuery(query).getResultList();
        return users;
	}


	@Override
	public Users find(String name) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Users> query = builder.createQuery(Users.class);
        Root<Users> u = query.from(Users.class);
        query.select(u).where(builder.like(u.get("username"), (name)));
        List<Users> users = entityManager.createQuery(query).getResultList();
        Users user = users.get(0);
        return user;
	}


	@Override
	public List<Integer> getAllUserIds() throws Exception {
		Query query = entityManager.createNativeQuery("select u.id from Users u where u.groupname='normalUser'");
		List<Integer> ids = query.getResultList();
		return ids;
	}

}
