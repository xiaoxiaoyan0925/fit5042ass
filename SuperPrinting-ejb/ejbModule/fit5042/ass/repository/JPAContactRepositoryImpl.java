package fit5042.ass.repository;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import fit5042.ass.repository.entities.Contact;
import fit5042.ass.repository.entities.Customer;

/**
 * 
 * @author Yan
 *
 */

@Stateless
public class JPAContactRepositoryImpl implements ContactRepository{

	@PersistenceContext(unitName = "Ass-ejbPU")
    private EntityManager entityManager;

	@Override
	public List<Contact> getAllContacts() throws Exception {
		return entityManager.createNamedQuery(Contact.GET_ALL_QUERY_NAME).getResultList();
	}

	@Override
	public void setContactList(List<Contact> contactList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeContact(int contactId) throws Exception {
		Contact contact = entityManager.find(Contact.class, contactId);
        entityManager.remove(contact);
		
	}

	@Override
	public void addContact(Contact contact) throws Exception {
		List<Contact> contacts =  entityManager.createNamedQuery(Contact.GET_ALL_QUERY_NAME).getResultList(); 
		contact.setContactId(contacts.get(0).getContactId() + 1);
        entityManager.persist(contact);
		
	}

	@Override
	public void editContact(Contact contact) throws Exception {
		entityManager.merge(contact);
		
	}

	@Override
	public int getContactId() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Contact searchContactById(int id) throws Exception {
		Contact contact = entityManager.find(Contact.class, id);
        return contact;
	}

	@Override
	public List<Contact> searchContactByName(String name) throws Exception {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Contact> query = builder.createQuery(Contact.class);
        Root<Contact> c = query.from(Contact.class);
        query.select(c).where(builder.like(c.get("name"), ("%"+name+"%")));
        List<Contact> contacts = entityManager.createQuery(query).getResultList();
        return contacts;
	}

	@Override
	public List<Contact> searchContactByCustomerId(int id) throws Exception {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Contact> query = builder.createQuery(Contact.class);
        Root<Contact> c = query.from(Contact.class);
        Customer customer = entityManager.find(Customer.class, id);
        query.select(c).where(builder.equal(c.get("customer"), customer));
        List<Contact> contacts = entityManager.createQuery(query).getResultList();
        return contacts; 
        
		
	}

}
