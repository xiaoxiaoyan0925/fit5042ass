package fit5042.ass.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import fit5042.ass.repository.entities.Industrytype;

/**
 * 
 * @author Yan
 *
 */

@Stateless
public class JPAIndustrytypeRepositoryImpl implements IndustrytypeRepository{

	@PersistenceContext(unitName = "Ass-ejbPU")
    private EntityManager entityManager;
	
	@Override
	public List<Industrytype> getAllIndustrytypes() throws Exception {
		return entityManager.createNamedQuery(Industrytype.GET_ALL_QUERY_NAME).getResultList();
	}

	@Override
	public void removeIndustrytype(int id) throws Exception {
		Industrytype industrytype = entityManager.find(Industrytype.class,id);
        entityManager.remove(industrytype);	
		
	}

	@Override
	public void addIndustrytype(Industrytype industrytype) throws Exception {
		List<Industrytype> industrytypes =  entityManager.createNamedQuery(Industrytype.GET_ALL_QUERY_NAME).getResultList(); 
		industrytype.setId(industrytypes.get(0).getId() + 1);
        entityManager.persist(industrytype);
		
	}

	@Override
	public void editIndustrytype(Industrytype industrytype) throws Exception {
		entityManager.merge(industrytype);
		
	}

	@Override
	public List<Industrytype> searchIndustrytypeByName(String name) throws Exception {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Industrytype> query = builder.createQuery(Industrytype.class);
        Root<Industrytype> i = query.from(Industrytype.class);
        query.select(i).where(builder.like(i.get("typename"), ("%"+name+"%")));
        List<Industrytype> industrytypes = entityManager.createQuery(query).getResultList();
        return industrytypes;
	}

	@Override
	public List<String> getAllIndustrytypeNames() throws Exception {
		Query query = entityManager.createNativeQuery("select i.typename from Industrytype i");
		List<String> typeNames = query.getResultList();
		return typeNames;
	}
	
	

}
