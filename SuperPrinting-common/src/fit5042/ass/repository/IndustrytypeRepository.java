package fit5042.ass.repository;

import java.util.List;

import javax.ejb.Remote;

import fit5042.ass.repository.entities.Industrytype;

/**
 * 
 * @author Yan
 *
 */

@Remote
public interface IndustrytypeRepository {


	public List<Industrytype> getAllIndustrytypes() throws Exception;
	
	public List<String> getAllIndustrytypeNames() throws Exception;

	public void removeIndustrytype(int id) throws Exception;
    
    public void addIndustrytype(Industrytype industrytype) throws Exception;
    
    public void editIndustrytype(Industrytype industrytype) throws Exception;
	

	public List<Industrytype> searchIndustrytypeByName(String name) throws Exception;
	
	
	
	
}