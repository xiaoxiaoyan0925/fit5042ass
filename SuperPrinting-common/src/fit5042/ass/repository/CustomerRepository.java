package fit5042.ass.repository;

import java.util.List;
import javax.ejb.Remote;
import fit5042.ass.repository.entities.*;

/**
 * 
 * @author Yan
 *
 */

@Remote
public interface CustomerRepository {


	public List<Customer> getAllCustomers() throws Exception;
	
	public List<DataCommunicationCustomer> getAllDcCustomers() throws Exception;
	
	public List<HealthCustomer> getAllHeCustomers() throws Exception;

	public void setCustomerList(List<Customer> customerList);
	
	public void removeCustomer(int customerId) throws Exception;
    
    public void addCustomer(Customer customer) throws Exception;
    
    public void editCustomer(Customer customer) throws Exception;

	public int getCustomerId();
	
	public Customer searchCustomerById(int customerId) throws Exception;
	
	public List<Customer> searchCustomerByUserid(int userid) throws Exception;
	
	public List<Contact> getAllCustomerContacts() throws Exception;

	public List<Customer> searchCustomerByName(String name) throws Exception;
	
	public List<Customer> searchCustomerByMultipleField(String fiele1, String value1, String field2, String value2) throws Exception;
	
	
	
	
}
