package fit5042.ass.repository;


import java.util.List;
import javax.ejb.Remote;
import fit5042.ass.repository.entities.*;

/**
 * 
 * @author Yan
 *
 */

@Remote
public interface UserRepository {


	public List<Users> getAllUsers() throws Exception;
	
	public List<Users> getAllNormalUsers() throws Exception;

	public void setUserList(List<Users> userList);
	
	public void removeUser(int userId) throws Exception;
    
    public void addUser(Users user) throws Exception;
    
    public void editUser(Users user) throws Exception;

	public int getUserId();
	
	public Users searchUserById(int userId) throws Exception;

	public List<Users> searchUserByName(String name) throws Exception;

	public Users find(String name);
	
	public List<Integer> getAllUserIds() throws Exception;
	
	
	
	
}
