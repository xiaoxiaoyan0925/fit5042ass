package fit5042.ass.repository;

import java.util.List;
import javax.ejb.Remote;
import fit5042.ass.repository.entities.*;

/**
 * 
 * @author Yan
 *
 */

@Remote
public interface ContactRepository {


	public List<Contact> getAllContacts() throws Exception;

	public void setContactList(List<Contact> contactList);
	
	public void removeContact(int contactId) throws Exception;
    
    public void addContact(Contact contact) throws Exception;
    
    public void editContact(Contact contact) throws Exception;

	public int getContactId();
	
	public Contact searchContactById(int id) throws Exception;
	
	public List<Contact> searchContactByCustomerId(int id) throws Exception;

	public List<Contact> searchContactByName(String name) throws Exception;
	
	
}