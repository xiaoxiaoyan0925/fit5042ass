package fit5042.ass.repository.entities;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity(name = "datacommunication_customer")
@DiscriminatorValue(value = "Data Communication")
@PrimaryKeyJoinColumn(name = "customer_id")
@NamedQueries({
    @NamedQuery(name = DataCommunicationCustomer.GET_ALL_QUERY_NAME, query =
    		    "SELECT d FROM datacommunication_customer d order by d.customerId asc")})

public class DataCommunicationCustomer extends Customer{
	
	private int customerId;
	private String creditLevel;
	
	public static final String GET_ALL_QUERY_NAME = "datacommunication_customer.getAll";
	
	@Id
	@Column(name = "customer_id")
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	
	@Column(name = "credit_level")
	public String getCreditLevel() {
		return creditLevel;
	}
	public void setCreditLevel(String creditLevel) {
		this.creditLevel = creditLevel;
	}
	
	@Override
	public String toString() {
		return "DataCommunicationCustomer [customerId=" + customerId + ", creditLevel=" + creditLevel + "]";
	}
	
	
	
}
