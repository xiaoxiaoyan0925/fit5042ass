package fit5042.ass.repository.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author Yan
 *
 */

@Entity
@Table(name = "ORDER")
@NamedQueries({@NamedQuery(name = Order.GET_ALL_QUERY_NAME,
  				query = "SELECT o FROM Order o order by o.orderId asc")})
public class Order implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String GET_ALL_QUERY_NAME = "Order.getAll";

	private int orderId;
	private Date date;
	private double amount;
	private String description;
	
	private Customer customer;
	
	public Order() {
	}

	public Order(int orderId, Date date, double amount, String description, Customer customer) {
		super();
		this.orderId = orderId;
		this.date = date;
		this.amount = amount;
		this.description = description;
		this.customer = customer;
	}

	@Id
    @GeneratedValue
    @Column(name = "order_id")
	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	@Column(name = "date")
	@Temporal(TemporalType.DATE)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Column(name = "amount")
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="customer_id", nullable=false)
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", date=" + date + ", amount=" + amount + ", description=" + description
				+ ", customer=" + customer + "]";
	}
	
	
}
