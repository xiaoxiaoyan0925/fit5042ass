package fit5042.ass.repository.entities;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.sun.el.parser.ParseException;

/**
 * 
 * @author Yan
 *
 */

@Entity
@Table(name = "CUSTOMER_CONTACT")
@NamedQueries({@NamedQuery(name = Contact.GET_ALL_QUERY_NAME, query = 
							"SELECT c FROM Contact c order by c.contactId asc")})

public class Contact implements Serializable{
	
	public static final String GET_ALL_QUERY_NAME = "Contact.getAll";

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contact_id")
	private int contactId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "gender")
	private String gender;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "position")
	private String position;
	
	@Column(name = "phone_number")
	private String phoneNumber;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "birthdate")
	private Date birthdate;
	
	@ManyToOne
	@JoinColumn(name="customer_id", nullable=false)
	private Customer customer;
	
	public Contact() {
	}

	public Contact(int contactId, String name, String gender, String title, String position,
			String phoneNumber, String email,Date birthdate, Customer customer) {
		super();
		this.contactId = contactId;
		this.name = name;
		this.gender = gender;
		this.title = title;
		this.position = position;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.birthdate = birthdate;
		this.customer = customer;
	}

	
	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "Contact [contactId=" + contactId + ", name=" + name + ", gender=" + gender + ", title=" + title
				+ ", position=" + position + ", phoneNumber=" + phoneNumber + ", email=" + email + ", birthdate=" + birthdate + ", customer="
				+ customer + "]";
	}

	
	private Date parseDate(String date) throws Exception {
	        return new Date((new SimpleDateFormat("yyyy-MM-dd")).parse(date).getTime());
	}
	
	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	
	
	
}
