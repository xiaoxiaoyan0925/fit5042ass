package fit5042.ass.repository.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the INDUSTRYTYPE database table.
 * 
 */
@Entity
@Table(name = "industrytype")
@NamedQueries({
	@NamedQuery(name = Industrytype.GET_ALL_QUERY_NAME, query=
				"SELECT i FROM Industrytype i order by i.typename asc")})

public class Industrytype implements Serializable {
	

	public static final String GET_ALL_QUERY_NAME = "Industrytype.getAll";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String typename;

	public Industrytype() {
	}
	

	public Industrytype(int id, String typename) {
		super();
		this.id = id;
		this.typename = typename;
	}



	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "typename")
	public String getTypename() {
		return this.typename;
	}

	public void setTypename(String typename) {
		this.typename = typename;
	}

}