package fit5042.ass.repository.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.*;

/**
 * 
 * @author Yan
 *
 */

@Entity
@Table(name = "users")
@NamedQueries({
    @NamedQuery(name = Users.GET_ALL_QUERY_NAME, query =
    		    "SELECT u FROM Users u order by u.id asc")})

public class Users implements Serializable{
	
	public static final String GET_ALL_QUERY_NAME = "users.getAll";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@NotBlank
	@Column(name = "username")
	private String username;
	
	@NotBlank
	@Column(name = "password")
	private String password;
	
	@NotBlank
	@Column(name = "groupname")
	private String groupname;
	
	public Users() {
	}
	
	public Users(int id, String username, String password, String groupname) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.groupname = groupname;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	
	
	
}