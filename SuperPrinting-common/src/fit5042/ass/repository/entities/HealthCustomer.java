package fit5042.ass.repository.entities;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity(name = "health_customer")
@DiscriminatorValue(value = "Health")
@PrimaryKeyJoinColumn(name = "customer_id")
@NamedQueries({
    @NamedQuery(name = HealthCustomer.GET_ALL_QUERY_NAME, query =
    		    "SELECT d FROM health_customer d order by d.customerId asc")})

public class HealthCustomer extends Customer{
	
	private int customerId;
	private String balance;
	
	public static final String GET_ALL_QUERY_NAME = "health_customer.getAll";
	
	@Id
	@Column(name = "customer_id")
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	
	@Column(name = "balance")
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "HealthCustomer [customerId=" + customerId + ", balance=" + balance + "]";
	}
	
	
	
}
