package fit5042.ass.repository.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 * 
 * @author Yan
 *
 */

@Entity
@Inheritance(strategy= InheritanceType.JOINED)
@DiscriminatorColumn(name = "industry_type", discriminatorType = DiscriminatorType.STRING, length=20)
@NamedQueries({
    @NamedQuery(name = Customer.GET_ALL_QUERY_NAME, query =
    		    "SELECT c FROM Customer c order by c.customerId asc")})

public class Customer implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String GET_ALL_QUERY_NAME = "Customer.getAll";

	private int customerId;
	private String name;
	private String phoneNumber;
	private String email;
	private String fax;
	
	private String industryType;
	private String description;
	private int userid;
	
	private Address address;
	
	private List<Contact> contacts;
	
	public void addContact(Contact contact) {
		contacts.add(contact);
		contact.setCustomer(this);
    }
 
    public void removeContact(Contact contact) {
    	contacts.remove(contact);
    	contact.setCustomer(null);
    }
	
	//private List<Order> orders;
	
	public Customer() {
	}

	public Customer(int customerId, String name, String phoneNumber, String email, String fax, String industryType,
			String desc, Address address, int userid, List<Contact> contacts/*, List<Order> orders */) {
		super();
		this.customerId = customerId;
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.fax = fax;
		this.industryType = industryType;
		this.description = desc;
		this.address = address;
		this.userid = userid;
		
		this.contacts = contacts;
		//this.orders = orders;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "customer_id")
	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Column(name = "industry_type")
	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String desc) {
		this.description = desc;
	}

	@Embedded
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + customerId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (customerId != other.customerId)
			return false;
		return true;
	}

	@OneToMany(cascade = CascadeType.ALL , fetch = FetchType.EAGER, mappedBy = "customer")
	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	/*
	@OneToMany(mappedBy = "customer")
	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	*/

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", name=" + name + ", phoneNumber=" + phoneNumber + ", email="
				+ email + ", fax=" + fax + ", industryType=" + industryType + ", description=" + description
				+ ", userid=" + userid + ", address=" + address + "]";
	}
	

	


	
	
	
}
